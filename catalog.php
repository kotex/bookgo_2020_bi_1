<?php 
session_start(); //запуск сессии

include 'functions.php'; //подключение файла с функциями

$kategory_id = 0; //объявление переменной с выбранным жанром
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Каталог</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="style-catalog.css">
    <link rel="stylesheet" href="media-styles.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
    integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div class="container animate__animated animate__fadeIn">
        <div class="top">
<?php include 'header_top.php'; //подключение хедера сайта ?>
            <div class="top_title animate__animated animate__slideInLeft">Каталог</div>
        </div>
        <div class="catalog_genres">
            <div class="catalog_genres_titles">
                <a href="catalog.php" class="list_title">Все жанры</a>
                <a href="catalog.php?action=classic" class="list_title">Классика</a>
                <a href="catalog.php?action=detective" class="list_title">Детектив</a>
                <a href="catalog.php?action=roman" class="list_title">Роман</a>
                <a href="catalog.php?action=fantasy" class="list_title">Фэнтези</a>
            </div>
            <div class="catalog_genres_books">
                <div class="twelve_books">
                    <div class="six_books">
                        <?php
                            if ($_GET['action'] == "classic") $kategory_id = 1;
                            if ($_GET['action'] == "detective") $kategory_id = 2;
                            if ($_GET['action'] == "roman") $kategory_id = 3;
                            if ($_GET['action'] == "fantasy") $kategory_id = 4; //в зависимости от того, какой жанр выбрал пользователь, переменной жанра присваивается соответствующее значение
                            $check = catalog_image_generator($kategory_id); //вызов функции генерации картинок объявлений соответствующего жанра
                            if ($check === false): // если книг, с выбранным жанром нет, то выводится соответствующее сообщение
                         ?>
                <h2>Ничего не нашлось</h2>
            <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
<?php include 'footer.php'; //подключение футера сайта ?>