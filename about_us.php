<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>О нас</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="contacts-style.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="about-style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
    integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link rel="stylesheet" href="style-catalog.css">
    <link rel="stylesheet" href="aut-style.css">
    <link rel="stylesheet" href="media-styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div class="container animate__animated animate__fadeIn">
        <div class="top">
<?php include 'header_top.php' ?>
            <div class="top_title animate__animated animate__slideInLeft">О нас</div>
        </div>
        <div class="introduction">
        <div class="about_us">Наш проект</div>
        <p class="text_about"> BookGo - сайт для обмена книгами между людьми. Книги сейчас стоят дорого и не каждый может себе их позволить. Наш проект предлагает людям отдавать книги, которые они уже прочитали, и находить те, которые они хотели бы прочитать совершенно бесплатно!</p>
        </div>
        <div class="rules">
            <div class="rule">
                <img src="img/1.png" alt="" class="rule-img">
                <div class="rule-1">
                    <p class="title_rule">Относись к другим так, как хотел бы, чтобы относились к тебе</p>
                    <p class="rule-main">Это правило говорит нам о том, что с людьми нужно общаться уважительно. На данном сайте пользователи ДОБРОВОЛЬНО делятся своими книгами, чтобы вы тоже смогли окунуться в историю этих произведений.</p>
                </div>
            </div>
            <div class="rule rule-change">
                <div class="rule-1">
                    <p class="title_rule">Будьте осторожны при встрече с незнакомыми людьми</p>
                    <p class="rule-main">Данный сайт не только поможет вам окунуться в мир литературы, но и поможет обрести друзей по интересами.Но не стоит забывать об осторожности. Не сообщайте личных данных тому, с кем планируете встретиться.</p>
                </div>
                <img src="img/2.png" alt="" class="rule-img">
            </div>
            <div class="rule">
                <img src="img/3.png" alt="" class="rule-img">
                <div class="rule-1">
                    <p class="title_rule">Возвращайте книги вовремя</p>
                    <p class="rule-main">Наш сайт основывается на доверии пользователей. Если человек одолжил вам свою книгу и вы четко обговорили сроки возврата, вы должны их придерживаться.</p>
                </div>
            </div>
        </div>
        <div class="team">
        <div class="about_us">Наша команда</div>
        <div class="contacts">
            <div class="contacts_inner">
                <div class="team_block">
                    <div class="block_inner">
                        <img src="img/team-1.jpg" alt="" class="team-member">
                        <img src="img/team-11.jpg" alt="" class="team-member-1">
                        <div class="contact_title">Шуравин Григорий</div>
                        <div class="position_info">Руководитель отдела разработки</div>
                        <div class="connect">
                            <a href="https://vk.com/terklun" class="vk" target="blank"><i class="fab fa-vk"></i></a>
                        </div>
                    </div>
                </div>
                <div class="team_block">
                    <div class="block_inner">
                        <img src="img/team-3.jpg" alt="" class="team-member">
                        <img src="img/team-33.jpg" alt="" class="team-member-1">
                        <div class="contact_title">Елизарова Екатерина</div>
                        <div class="position_info">Руководитель проекта</div>
                        <div class="connect">
                            <a href="https://vk.com/katya.ushla" class="vk" target="blank"><i class="fab fa-vk"></i></a>
                        </div>
                    </div>
                </div>
                <div class="team_block">
                    <div class="block_inner">
                        <img src="img/team-2.jpg" alt="" class="team-member">
                        <img src="img/team-22.jpg" alt="" class="team-member-1">
                        <div class="contact_title">Суркова Анастасия</div>
                        <div class="position_info">Руководитель информационной службы</div>
                        <div class="connect">
                            <a href="https://vk.com/anastsur" class="vk" target="blank"><i class="fab fa-vk"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <?php include 'footer.php'; ?>