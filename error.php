<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
    integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">    
    <link rel="stylesheet" href="error.css">
    <title>Упс...</title>
</head>
<body>
    <div class="container">
    <div class="main_error">
        <div class="pic_error"><i class="fas fa-exclamation-triangle"></i></div>
        <div class="text_error">Ой...что-то пошло не так...</div>
        <div class="type_error"><?=$_SESSION['err']?></div>
        <a href="index.php" class="btn effect btn_back">Вернуться на главную</a>
    </div>
</div>
</body>
</html>