<?php session_start(); //запуск сессии
$user_id = 13; //id пользователя, который создал данное объявление
$book_id = 13; //id данного объявления о книге

include '../functions.php'; //подключение файла с функциями
if (isset($_POST['change'])) change_status($book_id); //вызов функции смены статуса, если была нажата кнопка смены статуса
if (isset($_POST['delete'])) ad_delete($book_id); //вызов функции удаления страницы объявления, если была нажата кнопка удаления
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Лисья нора</title>
<?php include 'book_header.php'; //подключение хедера сайта?>
        <div class="book_content">
            <div class="book_picture animate__animated animate__slideInLeft">
                <div class="main_picture">
                    <img src="../img/4942781043_detail.jpg" alt="book" class="main_picture_img">
                </div>
            </div>
            <div class="book_describtion animate__animated animate__slideInRight">
                <div class="describtion_title">
                    <div class="book_name">Лисья нора</div>
                    <?php if ($status == 1): //провека статуса книги?>
                        <div class="text_up_2" style="color: green;">Свободна</div>
                    <?php else: ?>
                    <div class="text_up_2" style="color: red;">Занята</div>
                <?php endif; ?>
                </div>
                <div class="book_genre">Детектив</div>
                <?php if($_SESSION['id'] != $user_id): //если страница не принадлежит пользователю, то будет доступна кнопка для связи с хозяином книги?>
                    <a href="../user_profile/user_13.php" class="describtion_btn ef">Связаться<i class="fas fa-angle-right"></i></a>
                <?php else: //если же принадлежит, то будут доступны кнопки для смены статуса объявления и удаления страницы объявления?>
                    <form method="post">
                        <button type="submit" name="change" class="extra_btn dop_eff">Изменить статус</button>
                    </form>       
                <?php endif; ?>
                <div class="describtion_text"><p>Сенсационная трилогия писательницы Норы Сакавич «Все ради игры» была впервые опубликована в интернете и молниеносно покорила читателей во всем мире. «Лисья нора» повествует о команде «Лисов» — игроков экси (вымышленный спорт), которые, будучи отбросами в жизни, пытаются подняться со дна турнирной таблицы и выиграть чемпионат страны. Главный герой, Нил Джостен, скрывается от своего темного прошлого, однако, став частью команды, вынужден сражаться не только с соперниками, но и с новоиспеченными товарищами, каждый из которых хранит свои секреты.</p></div>
                <?php if($_SESSION['id'] == $user_id): //если же принадлежит, то будут доступны кнопки для смены статуса объявления и удаления страницы объявления?>
                <form method="post">
                    <button type="submit" name="delete" class="extra_btn dop_eff">Удалить объявление</button>
                </form>    
            <?php endif; ?>
            </div>
        </div>
<?php 
include 'comments.php'; //подключение файла с комментариями
include '../footer_down.php'; //подключение футера сайта
?>