<?php session_start(); //запуск сессии
$user_id = 11; //id пользователя, который создал данное объявление
$book_id = 12; //id данного объявления о книге

include '../functions.php'; //подключение файла с функциями
if (isset($_POST['change'])) change_status($book_id); //вызов функции смены статуса, если была нажата кнопка смены статуса
if (isset($_POST['delete'])) ad_delete($book_id); //вызов функции удаления страницы объявления, если была нажата кнопка удаления
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ведьмак</title>
<?php include 'book_header.php'; //подключение хедера сайта?>
        <div class="book_content">
            <div class="book_picture animate__animated animate__slideInLeft">
                <div class="main_picture">
                    <img src="../img/2112witcher.jpg" alt="book" class="main_picture_img">
                </div>
            </div>
            <div class="book_describtion animate__animated animate__slideInRight">
                <div class="describtion_title">
                    <div class="book_name">Ведьмак</div>
                    <?php if ($status == 1): //провека статуса книги?>
                        <div class="text_up_2" style="color: green;">Свободна</div>
                    <?php else: ?>
                    <div class="text_up_2" style="color: red;">Занята</div>
                <?php endif; ?>
                </div>
                <div class="book_genre">Детектив</div>
                <?php if($_SESSION['id'] != $user_id): //если страница не принадлежит пользователю, то будет доступна кнопка для связи с хозяином книги?>
                    <a href="../user_profile/user_11.php" class="describtion_btn ef">Связаться<i class="fas fa-angle-right"></i></a>
                <?php else: //если же принадлежит, то будут доступны кнопки для смены статуса объявления и удаления страницы объявления?>
                    <form method="post">
                        <button type="submit" name="change" class="extra_btn dop_eff">Изменить статус</button>
                    </form>       
                <?php endif; ?>
                <div class="describtion_text"><p>Одна из лучших фэнтези-саг за всю историю существования жанра. Оригинальное, масштабное эпическое произведение, одновременно и свободное от влияния извне, и связанное с классической мифологической, легендарной и сказовой традицией. Шедевр не только писательского мастерства Анджея Сапковского, но и переводческого искусства Евгения Павловича Вайсброта. "Сага о Геральте" - в одном томе. Бесценный подарок и для поклонника прекрасной фантастики, и для ценителя просто хорошей литературы. Перед читателем буквально оживает необычный, прекрасный и жестокий мир литературной легенды, в котором обитают эльфы и гномы, оборотни, вампиры и "низушки"-хоббиты, драконы и монстры, - но прежде всего ЛЮДИ.
Очень близкие нам, понятные и человечные люди - такие как мастер меча ведьмак Геральт, его друг, беспутный менестрель Лютик, его возлюбленная, прекрасная чародейка Йеннифэр, и приемная дочь - безрассудно отважная юная Цири…</p></div>
                <?php if($_SESSION['id'] == $user_id): //если же принадлежит, то будут доступны кнопки для смены статуса объявления и удаления страницы объявления?>
                <form method="post">
                    <button type="submit" name="delete" class="extra_btn dop_eff">Удалить объявление</button>
                </form>    
            <?php endif; ?>
            </div>
        </div>
<?php 
include 'comments.php'; //подключение файла с комментариями
include '../footer_down.php'; //подключение футера сайта
?>