<?php 
$link = mysqli_connect("localhost","root","",'book_go') 
    or die("Не удалось подключиться к MySQL " . mysqli_error($link)); //подключение к БД

$result = mysqli_query($link, "SELECT * FROM `book` WHERE `book_id` = '$book_id'");
$row = mysqli_fetch_array($result);

$status = $row['status']; //получение данных о статусе книги
?>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="../style-one-book.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <link rel="stylesheet" href="../style-catalog.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
    integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link rel="stylesheet" href="../media-styles.css">
    <link rel="stylesheet" href="comment-style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div class="container animate__animated animate__fadeIn">
        <div class="details_top">
            <div class="header">
                <a href="../index.php" class="logo"><i class="fas fa-book-open"></i>BookGo</a>
                <ul class="menu">
                    <li class="menu_list" ><a href="../index.php" class="menu_link">Главная</a></li>
                    <li class="menu_list"><a href="../about_us.php" class="menu_link">О нас</a></li>
                    <?php if ($_SESSION['id']==0): ?>
                    <li class="menu_list"><a href="../authorization_script.php" class="menu_link user">Каталог</a></li>
                    <?php else: ?>
                    <li class="menu_list"><a href="../catalog.php" class="menu_link">Каталог</a></li>
                    <?php endif; ?>
                    <li class="menu_list"><a href="../contacts.php" class="menu_link">Контакты</a></li>
                    <?php if ($_SESSION['id']==0): ?>
                    <li class="menu_list"><a href="../authorization_script.php" class="menu_link user"><i class="fas fa-user"></i></a></li>
                    <?php else: ?>
                    <li class="menu_list"><a href="../user_profile/user_<?=$_SESSION['id']?>.php" class="menu_link user"><i class="fas fa-user"></i></a></li>
                <?php endif; ?>
                </ul>
            </div>
        </div>
        <div class="genres_list">
            <div class="genres_list_inner">
                <div class="catalog_genres_titles" style="margin-left: 0%;">
                    <a href="../catalog.php" class="list_title">Все жанры</a>
                    <a href="../catalog.php?action=classic" class="list_title">Классика</a>
                    <a href="../catalog.php?action=detective" class="list_title">Детектив</a>
                    <a href="../catalog.php?action=roman" class="list_title">Роман</a>
                    <a href="../catalog.php?action=fantasy" class="list_title">Фэнтези</a>
                </div>
            </div>
        </div>