<?php session_start(); //запуск сессии
$user_id = 1; //id пользователя, который создал данное объявление
$book_id = 4; //id данного объявления о книге

include '../functions.php'; //подключение файла с функциями
if (isset($_POST['change'])) change_status($book_id); //вызов функции смены статуса, если была нажата кнопка смены статуса
if (isset($_POST['delete'])) ad_delete($book_id); //вызов функции удаления страницы объявления, если была нажата кнопка удаления
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Лягушки</title>
<?php include 'book_header.php'; //подключение хедера сайта?>
        <div class="book_content">
            <div class="book_picture animate__animated animate__slideInLeft">
                <div class="main_picture">
                    <img src="../img/418lyg.jpg" alt="book" class="main_picture_img">
                </div>
            </div>
            <div class="book_describtion animate__animated animate__slideInRight">
                <div class="describtion_title">
                    <div class="book_name">Лягушки</div>
                    <?php if ($status == 1): //провека статуса книги?>
                        <div class="text_up_2" style="color: green;">Свободна</div>
                    <?php else: ?>
                    <div class="text_up_2" style="color: red;">Занята</div>
                <?php endif; ?>
                </div>
                <div class="book_genre">Роман</div>
                <?php if($_SESSION['id'] != $user_id): //если страница не принадлежит пользователю, то будет доступна кнопка для связи с хозяином книги?>
                    <a href="../user_profile/user_1.php" class="describtion_btn ef">Связаться<i class="fas fa-angle-right"></i></a>
                <?php else: //если же принадлежит, то будут доступны кнопки для смены статуса объявления и удаления страницы объявления?>
                    <form method="post">
                        <button type="submit" name="change" class="extra_btn dop_eff">Изменить статус</button>
                    </form>       
                <?php endif; ?>
                <div class="describtion_text"><p>История Вань Синь - рассказ о том, что бывает, когда идешь на компромисс с совестью. Переступаешь через себя ради долга. Китай. Вторая половина XX века. Наша героиня - одна из первых настоящих акушерок, благодаря ей на свет появились сотни младенцев. Но вот наступила новая эра - государство ввело политику "одна семья - один ребенок". Страну обуял хаос. Призванная дарить жизнь, Вань Синь помешала появлению на свет множества детей и сломала множество судеб. Да, она выполняла чужую волю и действовала во имя общего блага. Но как ей жить дальше с этим грузом?</p></div>
                <?php if($_SESSION['id'] == $user_id): //если же принадлежит, то будут доступны кнопки для смены статуса объявления и удаления страницы объявления?>
                <form method="post">
                    <button type="submit" name="delete" class="extra_btn dop_eff">Удалить объявление</button>
                </form>    
            </div>
        </div>
<?php 
include 'comments.php'; //подключение файла с комментариями
include '../footer_down.php'; //подключение футера сайта
?>