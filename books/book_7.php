<?php session_start(); //запуск сессии
$user_id = 1; //id пользователя, который создал данное объявление
$book_id = 7; //id данного объявления о книге

include '../functions.php'; //подключение файла с функциями
if (isset($_POST['change'])) change_status($book_id); //вызов функции смены статуса, если была нажата кнопка смены статуса
if (isset($_POST['delete'])) ad_delete($book_id); //вызов функции удаления страницы объявления, если была нажата кнопка удаления
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Самая страшная книга. Пиковая дама</title>
<?php include 'book_header.php'; //подключение хедера сайта?>
        <div class="book_content">
            <div class="book_picture animate__animated animate__slideInLeft">
                <div class="main_picture">
                    <img src="../img/30232789124_detail.jpg" alt="book" class="main_picture_img">
                </div>
            </div>
            <div class="book_describtion animate__animated animate__slideInRight">
                <div class="describtion_title">
                    <div class="book_name">Самая страшная книга. Пиковая дама</div>
                    <?php if ($status == 1): //провека статуса книги?>
                    <div class="text_up_2" style="color: green;">Свободна</div>
                    <?php else: ?>
                    <div class="text_up_2" style="color: red;">Занята</div>
                <?php endif; ?>
                </div>
                <div class="book_genre">Фэнтези</div>
                <?php if($_SESSION['id'] != $user_id): //если страница не принадлежит пользователю, то будет доступна кнопка для связи с хозяином книги?>
                    <a href="../user_profile/user_1.php" class="describtion_btn ef">Связаться<i class="fas fa-angle-right"></i></a>
                <?php else: //если же принадлежит, то будут доступны кнопки для смены статуса объявления и удаления страницы объявления?>
                    <form method="post">
                        <button type="submit" name="change" class="extra_btn dop_eff">Изменить статус</button>
                    </form>       
                <?php endif; ?>
                <div class="describtion_text"><p>«Пиковая Дама, приди!»
Слова, которые, как гласит старинное поверье, надо произнести несколько раз, стоя перед зеркалом, чтобы призвать таинственный призрак. Мистический обряд, который многие воспринимают как шутку, веселое и немножечко жуткое развлечение. Но с зеркалами не шутят, ведь Зазеркалье — это мир мертвых. И горе тем, кто играет с духами умерших.
Два романа под одной обложкой. Две книги в одной — две новинки от Максима Кабира, лауреата премий «Рукопись года» и «Мастера ужаса», автора книг «Скелеты» и «Призраки». Две истории о том, как обычные подростки попытались вызвать Пиковую Даму — и к каким ужасным последствиям это привело. История самой Дамы — мстительного призрака, который не остановится, пока не заберет души тех, кто его позвал.
Читайте на свой страх и риск. И — не смотрите в зеркала…</p></div>
                <?php if($_SESSION['id'] == $user_id): //если же принадлежит, то будут доступны кнопки для смены статуса объявления и удаления страницы объявления?>
                <form method="post">
                    <button type="submit" name="delete" class="extra_btn dop_eff">Удалить объявление</button>
                </form>    
            <?php endif; ?>
            </div>
        </div>
<?php 
include 'comments.php'; //подключение файла с комментариями
include '../footer_down.php'; //подключение футера сайта
?>