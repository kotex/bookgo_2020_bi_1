<?php
session_start(); //запуск сессии

include 'ad_create_functions.php'; //подключение файла с функциями, которые используются в создании объявления

if(isset($_FILES['file'])) upload(); //если файл выбран и отправлен, то запускается функция загрузки файла

  $link = mysqli_connect("localhost","root","",'book_go') 
    or die("Не удалось подключиться к MySQL " . mysqli_error($link)); //подключение к базе данных

  if (isset($_POST['header']) 
    && isset($_POST['text']) 
    && isset($_POST['kategory'])) //проверка отправлены ли данные из формы
  {
    $header=$_POST['header'];
    $text=$_POST['text'];
    $kategory_id=$_POST['kategory'];
    $status='1';
    $user_id=$_SESSION['id']; //присваивание данных из суперглобальных массивов обычным переменным

    $result= mysqli_query($link,"INSERT INTO `book`(`user_id`, `kategory_id`, `header`, `text`, `date`, `status`) VALUES ('$user_id','$kategory_id','$header','$text',NOW(),'$status')") or die ("Ошибка " . mysqli_error($link)); //внесение данных в БД

    if ($result == true) //проверка получилось ли внести данные в БД
      {
      	$result = mysqli_query($link, "SELECT * FROM `book` WHERE `header`='$header'");
      	$row = mysqli_fetch_array($result);

      	$book_id = $row['book_id']; //получение id объявления о книге, которое только что было создано в БД

      	$result = mysqli_query($link,"UPDATE `picture` SET `book_id`='$book_id' WHERE `picture_id`= '$picture_id'") or die ("Ошибка " . mysqli_error($link)); //привязка id объявления о книге к картинке этой книги

      	creation_book_page(); //вызов функции создания файла с объявлением о книге
      	
        header('location: http://localhost/bookgo_2020_bi_1/index.php'); //переадресация пользователя на главную страницу
      }
    else //в случае, если создать объявление о книге в БД не получилось, выводится соответствующее сообщение
      {
        $_SESSION['err'] = 'Создание объявления провалилось!';
        header('location: http://localhost/bookgo_2020_bi_1/error.php'); //переадресация на страницу с ошибкой
      }
  }

include 'ad_create_view.php'; //подключение вида страницы создания объявления о книге
?>