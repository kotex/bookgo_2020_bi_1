<?php 
session_start(); //запуск сессии
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Контакты</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="contacts-style.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
    integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link rel="stylesheet" href="style-catalog.css">
    <link rel="stylesheet" href="aut-style.css">
    <link rel="stylesheet" href="media-styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div class="container animate__animated animate__fadeIn">
        <div class="top">
<?php include 'header_top.php'; //подключение файла с хедером сайта ?>
            <div class="top_title animate__animated animate__slideInLeft">Контакты</div>
        </div>
        <div class="contacts">
            <div class="contacts_inner">
                <div class="contacts_block">
                    <div class="block_inner">
                        <div class="contact_pin"><i class="fas fa-user-cog"></i></div>
                        <div class="contact_title">Шуравин Григорий</div>
                        <div class="position_info">Руководитель отдела разработки</div>
                        <div class="contact_phone">
                            <div class="contact_mini_title">Телефон:</div>
                            <div class="contact_info">892089208920</div>
                        </div>
                        <div class="contact_email">
                            <div class="contact_mini_title">Email:</div>
                            <div class="contact_info">bookgo</div>
                        </div>
                    </div>
                </div>
                <div class="contacts_block">
                    <div class="block_inner">
                        <div class="contact_pin"><i class="fas fa-user-tie"></i></div>
                        <div class="contact_title">Елизарова Екатерина</div>
                        <div class="position_info">Руководитель проекта</div>
                        <div class="contact_phone">
                            <div class="contact_mini_title">Телефон:</div>
                            <div class="contact_info">892089208920</div>
                        </div>
                        <div class="contact_email">
                            <div class="contact_mini_title">Email:</div>
                            <div class="contact_info">bookgo</div>
                        </div>
                    </div>
                </div>
                <div class="contacts_block">
                    <div class="block_inner">
                        <div class="contact_pin"><i class="fas fa-user-edit"></i></div>
                        <div class="contact_title">Суркова Анастасия</div>
                        <div class="position_info">Руководитель информационной службы</div>
                        <div class="contact_phone">
                            <div class="contact_mini_title">Телефон:</div>
                            <div class="contact_info">892089208920</div>
                        </div>
                        <div class="contact_email">
                            <div class="contact_mini_title">Email:</div>
                            <div class="contact_info">bookgo</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php include 'footer.php'; //подключение футера сайта ?>