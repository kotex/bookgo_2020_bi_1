<?php 
session_start(); //начало сессии
        
include 'functions.php'; //подключение файла с функциями

if ($_GET['action'] == "out") out(); //проверка нажата ли кнопка выхода. Если нажата, то запускается скрипт уничтожения сессии

if (empty($_SESSION['id'])) //проверка залогинен ли пользователь. Если не залогинен, то задается 0 id сессии, что означает, что пользователь гость
{
    $_SESSION['id'] = 0;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Главная</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="media-styles.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <link rel="stylesheet" href="aut-style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
    integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>    
<div class="container animate__animated animate__fadeIn">
    <div class="big_book">
        <div class="big_book_inner">
<?php include 'header.php'; //подключение файла с хедером ?>
            <div class="big_book_text margin_">
                <div class="animate__animated animate__slideInLeft">
                <div class="text_up">Наш проект позволит тебе</div>
                <div class="text_h2">Лучше узнать этот мир</div>
                <div class="text_under">И найти его невероятно интересным. Мы объединяем людей и книги, чтобы помочь вам найти то, что действительно нужно.</div>
                <a href="about_us.php" class="btn effect">Большe<i class="fas fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <?php if ($_SESSION['id']==0): //если пользователь гость, то ему нельзя будет перейти на страницу с каталогом и его отправит на страницу авторизации ?>
    <div class="genres">
        <div class="main_hat">
            <div class="hat_heading">Жанры</div>
            <a href="authorization_script.php" class="explore_all">Показать всё&nbsp;&rsaquo;</a>
        </div>
        <div class="genres_catalog">
            <div class="half">
                <div class="small_book">
                <a href="authorization_script.php" class="small_book_inner"><img src="img/ganre-1.jpg" alt="Классика"></a>
                <div class="small_book_text">
                    <div class="small_btn">&rsaquo;</div>
                    <div class="small_header">Перейти</div>
                    <div class="small_text">Посмотрите, что пользователи готовы предложить в разделе "Классика".</div>
                </div>
            </div>
                <div class="small_book">
                <a href="authorization_script.php" class="small_book_inner"><img src="img/ganre-2.jpg" alt="Детектив"></a>
                <div class="small_book_text">
                    <div class="small_btn">&rsaquo;</div>
                    <div class="small_header">Перейти</div>
                    <div class="small_text">Посмотрите, что пользователи готовы предложить в разделе "Детектив"</div>
                </div>
            </div>
            </div>
            <div class="half">
                <div class="small_book">
                <a href="authorization_script.php" class="small_book_inner"><img src="img/ganre-3.jpg" alt="Роман"></a>
                <div class="small_book_text">
                    <div class="small_btn">&rsaquo;</div>
                    <div class="small_header">Перейти</div>
                    <div class="small_text">Посмотрите, что пользователи готовы предложить в разделе "Роман"</div>
                </div>
            </div>
                <div class="small_book">
                <a href="authorization_script.php" class="small_book_inner"><img src="img/ganre-4.jpg" alt="Фэнтези"></a>
                <div class="small_book_text">
                    <div class="small_btn">&rsaquo;</div>
                    <div class="small_header">Перейти</div>
                    <div class="small_text">Посмотрите, что пользователи готовы предложить в разделе "Фэнтези"</div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <?php else: ?>
    <div class="genres">
        <div class="main_hat">
            <div class="hat_heading">Жанры</div>
            <a href="catalog.php" class="explore_all">Показать всё&nbsp;&rsaquo;</a>
        </div>
        <div class="genres_catalog">
            <div class="half">
                <div class="small_book">
                <a href="catalog.php?action=classic" class="small_book_inner"><img src="img/ganre-1.jpg" alt="Классика"></a>
                <div class="small_book_text">
                    <div class="small_btn">&rsaquo;</div>
                    <div class="small_header">Перейти</div>
                    <div class="small_text">Посмотрите, что пользователи готовы предложить в разделе "Классика".</div>
                </div>
            </div>
                <div class="small_book">
                <a href="catalog.php?action=detective" class="small_book_inner"><img src="img/ganre-2.jpg" alt="Детектив"></a>
                <div class="small_book_text">
                    <div class="small_btn">&rsaquo;</div>
                    <div class="small_header">Перейти</div>
                    <div class="small_text">Посмотрите, что пользователи готовы предложить в разделе "Детектив"</div>
                </div>
            </div>
            </div>
            <div class="half">
                <div class="small_book">
                <a href="catalog.php?action=roman" class="small_book_inner"><img src="img/ganre-3.jpg" alt="Роман"></a>
                <div class="small_book_text">
                    <div class="small_btn">&rsaquo;</div>
                    <div class="small_header">Перейти</div>
                    <div class="small_text">Посмотрите, что пользователи готовы предложить в разделе "Роман"</div>
                </div>
            </div>
                <div class="small_book">
                <a href="catalog.php?action=fantasy" class="small_book_inner"><img src="img/ganre-4.jpg" alt="Фэнтези"></a>
                <div class="small_book_text">
                    <div class="small_btn">&rsaquo;</div>
                    <div class="small_header">Перейти</div>
                    <div class="small_text">Посмотрите, что пользователи готовы предложить в разделе "Фэнтези"</div>
                </div>
            </div>
            </div>
        </div>
    </div>
<?php endif; ?>
    <?php if ($_SESSION['id']==0): //если пользователь гость, то ему нельзя будет перейти на страницу с каталогом и его отправит на страницу авторизации ?>
         <div class="catalog padding_">
        <div class="main_hat">
            <div class="hat_heading">Каталог</div>
            <a href="authorization_script.php" class="explore_all">Показать всё&nbsp;&rsaquo;</a>
        </div>
        <div class="six_books">
            <?php image_generator_unlog(); //вызов функции генерации картинок объявлений, если пользователь не авторизован ?>
        </div>
    </div>
    <?php else: //если пользователь авторизован, то ему будет доступен переход на страницу каталога ?>
    <div class="catalog padding_">
        <div class="main_hat">
            <div class="hat_heading">Каталог</div>
            <a href="catalog.php" class="explore_all">Показать всё&nbsp;&rsaquo;</a>
        </div>
        <div class="six_books">
            <?php image_generator(); //вызов функции генерации картинок, если пользователь авторизован ?>
        </div>
    </div>
    <?php endif; ?>
    <div class="hot">
       <div class="hot_inner">
            <div class="hot_text margin_">
                <div class="text_up_2">Заинтригованы?</div>
                <div class="text_h2">Книга недели</div>
                <div class="text_under">Здесь то, что заинтересовало наших пользователей больше всего</div>
                <a href="books/book_5.php" class="btn effect">Больше<i class="fas fa-angle-right"></i></a>
            </div>
            
       </div>
    </div>
    <div class="social">
        <div class="social_inner padding_">
            <div class="social_background"></div>
            <div class="social_text">
                <div class="text_up_3">Мобильное приложение</div>
                <div class="text_h2">Находи. Делись. Читай.</div>
                <div class="text_under_2">У тебя появилась возможность быть с нами всегда</div>
                <div class="two_social_btns">
                    <a href="#" class="AppStore_btn"><img src="img/AppStore.png" alt="AppStore"></a>
                    <a href="#" class="GooglePlay_btn"><img src="img/GooglePlay.png" alt="GooglePlay"></a>
                </div>
            </div>
        </div>
    </div>
<?php include 'footer.php'; //подключение файла с футером главной страницы ?>
</div>
</body>
</html>