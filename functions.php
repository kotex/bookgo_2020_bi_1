<?php 
function out() { //функция завершения сессии
	unset($_SESSION['id']); //убирается id сессии
	session_destroy(); //уничтожается сессия
}

function ad_delete($book_id) //функция удаления страницы объявления о книге
{
	$link = mysqli_connect("localhost","root","",'book_go') 
    or die("Не удалось подключиться к MySQL " . mysqli_error($link)); //подключение к БД

	$result = mysqli_query($link, "DELETE  FROM `comment` WHERE `book_id` = '$book_id'"); //удаление данных из таблицы с комментариями
	$result = mysqli_query($link, "DELETE  FROM `picture` WHERE `book_id` = '$book_id'"); //удаление данных из таблицы с картинками
	$result = mysqli_query($link, "DELETE  FROM `book` WHERE `book_id` = '$book_id'"); //удаление данных из таблицы с книгами

	$file = 'book_'.$book_id.'.php'; //переменной присваевается имя удаляемого файла

	unlink($file); //удаление файла с объявлением о книге

	header('location: ../index.php'); //переадресация на главную страницу
}

function change_status($book_id) //функция изменения статуса книги
{
	$link = mysqli_connect("localhost","root","",'book_go') 
    or die("Не удалось подключиться к MySQL " . mysqli_error($link)); //подключение к БД

    $result = mysqli_query($link, "SELECT * FROM `book` WHERE `book_id` = '$book_id'");
    $row = mysqli_fetch_array($result);

    $status = $row['status']; //получение значения статуса книги

    if ($status == 1) //если книга свободна(значение 1), то ей присваевается значение 0(книга занята). И наоборот, если книга занята
    {
    	$result = mysqli_query($link, "UPDATE `book` SET `status` = '0' WHERE `book_id` = '$book_id'");
    }
    else
    {
    	$result = mysqli_query($link, "UPDATE `book` SET `status` = '1' WHERE `book_id` = '$book_id'");
    }
}

function comments_generator($book_id) //функция для вывода комментариев
{
	$link = mysqli_connect("localhost","root","",'book_go') 
    or die("Не удалось подключиться к MySQL " . mysqli_error($link)); //подключение к БД

    $result = mysqli_query($link, "SELECT * FROM `comment` WHERE `book_id` = '$book_id'");
    $row = mysqli_fetch_assoc($result);

    if(!empty($row['comment_id'])) //проверка наличия комментариев под объявлением
    { 
    	$result = mysqli_query($link, "SELECT * FROM `comment` WHERE `book_id` = '$book_id'");

    	while ($row = mysqli_fetch_assoc($result)) //если комментарии присутствуют, то цикл их выводит
    	{
    		$text = $row['text'];
    		$user_id = $row['user_id']; //получение id пользователя и текста комментария из БД

    		$result_login = mysqli_query($link, "SELECT * FROM `users` WHERE `user_id` = '$user_id'");
    		$row_login = mysqli_fetch_assoc($result_login);

    		$login = $row_login['login']; //получение логина пользователя из БД

    		echo "<div class=\"full_comm\"><p class=\"user_comm\">".$login."</p>
					<p>".$text."</p></div>"; //вывод текста комментария и логина пользователя, оставившего комментарий
    	}
    }
    else
    {
    	echo "<p>Комментариев нет</p>"; //если комментариев нет, то выводится сообщение
    }
}

function catalog_image_generator($kategory_id) //функция для генерации картинок объявлений в каталоге
{
	$link = mysqli_connect("localhost","root","",'book_go') 
	    or die("Не удалось подключиться к MySQL " . mysqli_error($link)); //подключение к БД

	$result = mysqli_query($link, "SELECT * FROM `picture` WHERE 1");

	if ($kategory_id == 0) //если жанр не задан, то выводятся все картинки объявлений
	{
		while($row = mysqli_fetch_assoc($result)) //цикл вывода картинок объявлений
		{
			echo '<a href="books/book_'.$row['book_id'].'.php" class="one_of_six_books"><img src="'.$row['img'].'" alt="book" class="one_img"><a>';
		}
	}
	else //если же жанр задан, то выводятся только те картинки объявлений, которые относятся к данному жанру
	{
		$zap = mysqli_query($link, "SELECT * FROM `book` WHERE `kategory_id`='$kategory_id'");
		$r = mysqli_fetch_array($zap);

		if (!empty($r['book_id'])) //проверка на наличие книг в данном жанре
		{
			while($row = mysqli_fetch_assoc($result)) //цикл вывода картинок объявлений определенного жанра
			{
				$book_id = $row['book_id'];
				$res = mysqli_query($link, "SELECT * FROM `book` WHERE `book_id`='$book_id'"); //получение id жанра выбранной книги
			    $ro = mysqli_fetch_array($res);
			    $kategory = $ro['kategory_id'];

			    if ($kategory == $kategory_id) //если жанр выбранной книги совпадает с жанром, который выбрал пользователь, то картинка выводится
			    {
			    	echo '<a href="books/book_'.$row['book_id'].'.php" class="one_of_six_books"><img src="'.$row['img'].'" alt="book" class="one_img"><a>';
			    }
			}
		}
		else
		{
			return false; //если книг с данным жанром нет, то возвращается false и на странице выводится сообщение о том, что книг в этом жанре нет на сайте
		}
	}
}

function image_user_generator($user_id) //функция вывода картинок, опубликованных пользователем, на его странице
{
	$link = mysqli_connect("localhost","root","",'book_go') 
	    or die("Не удалось подключиться к MySQL " . mysqli_error($link)); //подключение к БД

	    $zap = mysqli_query($link, "SELECT * FROM `book` WHERE `user_id`='$user_id'"); //получение списка книг, которые опубликовал данный пользователь
	    $r = mysqli_fetch_array($zap);

	    if (!empty($r['book_id'])) //проверка есть ли книги, которые публиковал данный пользователь
	    {

		    $result = mysqli_query($link, "SELECT * FROM `picture` WHERE 1"); //получение данных о всех картинках

		    while ($row = mysqli_fetch_assoc($result)) //цикл вывода картинок
		    {
		    	$book_id = $row['book_id']; //получение id книги из таблицы с картинками
		    	$res = mysqli_query($link, "SELECT * FROM `book` WHERE `book_id`='$book_id'");
		    	$ro = mysqli_fetch_array($res); //получение данных о книге, которой принадлежит этот id
		    	$user = $ro['user_id'];

		    	if ($user == $user_id) //если id пользователя, опубликовавшего книгу совпадает с id пользователя, которому принадлежит страница пользователя, то картинка выводится
		    	{
		    		echo '<a href="../books/book_'.$row['book_id'].'.php" class="one_of_six_books"><img src="../'.$row['img'].'" alt="book" class="one_img"><a>';
		    	}
		    }
		}

		else //если книг нет, то функция возвращает false и выводится соответствующее сообщение
		{
			return false;
		}
}

function image_generator() //функция вывода картинок на главной странице для авторизованных пользователей
{
	$link = mysqli_connect("localhost","root","",'book_go') 
	    or die("Не удалось подключиться к MySQL " . mysqli_error($link)); //подключение к БД

	$result = mysqli_query($link, "SELECT * FROM `picture` WHERE 1"); //получение данных о всех картинках

	$k = 0; //объявление переменной счетчика количества выведенных картинок

	while($row = mysqli_fetch_assoc($result)) //цикл вывода картинок
	{
		if ($k < 6) //если картинок меньше 6, то они будут выводится
		{
			echo '<a href="books/book_'.$row['book_id'].'.php" class="one_of_six_books"><img src="'.$row['img'].'" alt="book" class="one_img"><a>'; //главным отличием от следующей функции является то, что данная функция выводит картинки с ссылками на объявление данной книги
			$k++;
		}
		else //если же картинок уже больше 6, то больше картинок выводится не будет
		{
			break;
		}
	}
}

function image_generator_unlog() //функция вывода картинок для незарегистрированных пользователей (все то же самое, что и в предыдущей функции, только пользователей при нажатии на картинку отправляет на страницу авторизации)
{
	$link = mysqli_connect("localhost","root","",'book_go') 
	    or die("Не удалось подключиться к MySQL " . mysqli_error($link)); //подключение к БД

	$result = mysqli_query($link, "SELECT * FROM `picture` WHERE 1");

	$k = 0;

	while($row = mysqli_fetch_assoc($result))
	{
		if ($k < 6)
		{
			echo '<a href="authorization_script.php" class="one_of_six_books"><img src="'.$row['img'].'" alt="book" class="one_img"><a>';
			$k++;
		}
		else
		{
			break;
		}
	}
}

function creation_user_page() //функция создания страницы пользователя
{
	session_start(); //запуск сессии

	$link = mysqli_connect("localhost","root","",'book_go') 
	    or die("Не удалось подключиться к MySQL " . mysqli_error($link)); //подключение к БД

	$user_id = $_SESSION['id'];

	$result = mysqli_query($link, "SELECT * FROM `users` WHERE `user_id`='$user_id' ");
	$row = mysqli_fetch_array($result); //получение данных о пользователе

	$login = $row['login'];
	$phone = $row['phone'];
	$email = $row['email'];

	$filename = 'user_profile/user_' . $user_id . '.php'; //задание имя файла и места расположение файла со страницей пользователя
	$text = '<?php 
session_start();
$user_id = '.$user_id.';
include \'../functions.php\';
include \'user_header.php\';
?>
        <div class="profile_main">
            <div class="profile_describtion animate__animated animate__zoomIn">
                <div class="main_photo">
                <img src="../img/user_logo.png" alt="photo" class="profile_photo">
                </div>
                <div class="describtion_title">
                    <div class="profile_name">'.$login.'</div>
                </div>
                <div class="ll">
                    <div class="phone_number">'.$phone.'</div>
                    <hr>
                    <div class="email">'.$email.'</div>
                </div>
                <?php if($_SESSION[\'id\'] == $user_id): ?>
                <div class="margin_book"><a href="../ad_create_script.php" class="extra_btn">Создать объявление</a></div>
                <div><a href="../index.php?action=out" class="extra_btn">Выйти</a></div>
            <?php endif; ?>
            </div>
        </div>
        <div class="catalog padding_">
            <div class="main_hat">
                <div class="hat_heading">Пользователь готов предложить эти книги</div>
            </div>
            <div class="six_books">
               <?php $check = image_user_generator($user_id); ?>
                <?php if ($check === false):?>
                <h2 class="no_books">Пользоветель пока не опубликовал ни одной книги</h2>
            <?php endif; ?>
            </div>
        </div>
<?php include \'../footer_down.php\'; ?>
'; //задание информации для внесения в файл страницы пользователя

file_put_contents($filename, $text); //создание файла со страницей пользователя
}
?>