            <div class="header">
                <a href="index.php" class="logo"><i class="fas fa-book-open"></i>    BookGo</a>
                <ul class="menu">
                    <li class="menu_list"><a href="index.php" class="menu_link">Главная</a></li>
                    <li class="menu_list"><a href="about_us.php" class="menu_link">О нас</a></li>
                    <?php if ($_SESSION['id']==0): //если пользователь не авторизован, то его отправит на страницу авторизации?>
                    <li class="menu_list"><a href="authorization_script.php" class="menu_link user">Каталог</a></li>
                    <?php else: //если авторизован, то на страницу каталога?>
                    <li class="menu_list"><a href="catalog.php" class="menu_link">Каталог</a></li>
                    <?php endif; ?>
                    <li class="menu_list"><a href="contacts.php" class="menu_link">Контакты</a></li>
                    <?php if ($_SESSION['id']==0): //если пользователь не авторизован, то его отправит на страницу авторизации?>
                    <li class="menu_list"><a href="authorization_script.php" class="menu_link user"><i class="fas fa-user"></i></a></li>
                    <?php else: //если авторизован, то на его страницу профиля?>
                    <li class="menu_list"><a href="user_profile/user_<?=$_SESSION['id']?>.php" class="menu_link user"><i class="fas fa-user"></i></a></li>
                <?php endif; ?>
                </ul>
            </div>