<?php 
session_start();
$user_id = 1;
include '../functions.php';
include 'user_header.php';
?>
        <div class="profile_main">
            <div class="profile_describtion animate__animated animate__zoomIn">
                <div class="main_photo">
                <img src="../img/adminushka.jpg" alt="photo" class="profile_photo">
                </div>
                <div class="describtion_title">
                    <div class="profile_name">adminushka</div>
                </div>
                <div class="ll">
                    <div class="phone_number">89991233211</div>
                    <hr>
                    <div class="email">god@yandex.ru</div>
                </div>
                <?php if($_SESSION['id'] == $user_id): ?>
                <div class="margin_book"><a href="../ad_create_script.php" class="extra_btn">Создать объявление</a></div>
                <div><a href="../index.php?action=out" class="extra_btn">Выйти</a></div>
            <?php endif; ?>
            </div>
        </div>
        <div class="catalog padding_">
            <div class="main_hat">
                <div class="hat_heading">Пользователь готов предложить эти книги</div>
            </div>
            <div class="six_books">
               <?php $check = image_user_generator($user_id); ?>
                <?php if ($check === false):?>
                <h2 class="no_books">Пользоветель пока не опубликовал ни одной книги</h2>
            <?php endif; ?>
            </div>
        </div>
<?php include '../footer_down.php'; ?>
