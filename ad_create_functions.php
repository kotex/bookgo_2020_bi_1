<?php
 function upload() //функция для проверки и загрузки файла
  {
	$file=$_FILES['file']; //присваивание файла из суперглобального массива в обычную переменную
	$check = can_upload($file); //вызов фукции, которая проверяет подходит ли файл для загрузки

      if($check === true) //если фукция проверки вернула true, то вызывается функция для отправки загрузки файла
      {
        make_upload($file); //вызов функции загрузки файла
      }
      else //если функция проверки не дает загрузить файл, то выводится сообщение об ошибке, которое вернула функция проверки
      {
        $_SESSION['err'] = $check;
        header('location: http://localhost/bookgo_2020_bi_1/error.php');  //переадресация на страницу с ошибкой
      }
  }

  function can_upload($file) //функция для проверки файла
  {
    if($file['name'] == '') //проверка выбран ли файл

		return 'Вы не выбрали файл.';

	if($file['size'] == 0) //проверка размера файла

		return 'Файл слишком большой.';

	$getMime = explode('.', $file['name']);
	$mime = strtolower(end($getMime));
	$types = array('jpg', 'png', 'jpeg');

	if(!in_array($mime, $types)) //проверка типа файла
		return 'Недопустимый тип файла.';

	return true;
  }

  function make_upload($file) //функция загрузки файла
  {
  	$link = mysqli_connect("localhost","root","",'book_go') 
    or die("Не удалось подключиться к MySQL " . mysqli_error($link)); //подклбчение к БД

  	$name = mt_rand(6, 10000) . $file['name']; //присвоение файлу уникального имени
	copy($file['tmp_name'], 'img/' . $name); //загрузка файла в папку с картинками
	$image='img/'. $name; //создание перменной с ссылкой на файл

	$result= mysqli_query($link,"INSERT INTO `picture`(`img`, `book_id`) VALUES ('$image','0')") or die ("Ошибка " . mysqli_error($link)); //загрузка ссылки на картинку в БД

	if ($result == true) //если загрузка удалась, то создается переменная $picture_id с id картинки из таблицы БД.
	{
		$result = mysqli_query($link, "SELECT * FROM `picture` WHERE `img`='$image'");
		$row = mysqli_fetch_array($result);
		global $picture_id; //видимость переменной глобальная, чтобы передать данные на страницу создания объявления о книге
		$picture_id = $row['picture_id'];
	}
  }

  function creation_book_page() //функция создания страницы объявления
  { 
  	$link = mysqli_connect("localhost","root","",'book_go') 
    or die("Не удалось подключиться к MySQL " . mysqli_error($link)); //подключение к БД

  	global $picture_id;

	$res = mysqli_query($link, "SELECT * FROM `picture` WHERE `picture_id`='$picture_id'");
    $row = mysqli_fetch_array($res);

    $book_id = $row['book_id']; //получение id книги

  	$result = mysqli_query($link, "SELECT * FROM `book` WHERE `book_id`='$book_id'");
    $row = mysqli_fetch_array($result);

    $header=$row['header'];
    $text=$row['text'];
    $kategory_id=$row['kategory_id'];
    $status=$row['status'];
    $user_id=$row['user_id']; //получение данных из бд, для создания страницы объявления

    if ($status != 0) //проверка статуса книги и задание соответствующего значения
    {
    	$status = 'Свободна';
    }
    else
    {
    	$status = 'Занята';
    }

    $result = $result = mysqli_query($link, "SELECT * FROM `users` WHERE `user_id`='$user_id'");
    $row = mysqli_fetch_array($result);

    $user_login = $row['login']; //получение id пользователя, который создал объявление о книге

    $result = $result = mysqli_query($link, "SELECT * FROM `kategory` WHERE `kategory_id`='$kategory_id'");
    $row = mysqli_fetch_array($result);

    $kategory = $row['name']; //получение жанра книги

    $result = $result = mysqli_query($link, "SELECT * FROM `picture` WHERE `book_id`='$book_id'");
    $row = mysqli_fetch_array($result);

    $img = $row['img']; //получение ссылки на картинку

    $filename = 'books/book_' . $book_id . '.php'; //создание имени и расположения файла страницы объявления о книге
    $text = '<?php session_start(); 
$user_id = '.$user_id.';
$book_id = '.$book_id.';
include \'../functions.php\'; 
if (isset($_POST[\'change\'])) change_status($book_id);
if (isset($_POST[\'delete\'])) ad_delete($book_id);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>'.$header.'</title>
<?php include \'book_header.php\'; ?>
        <div class="book_content">
            <div class="book_picture animate__animated animate__slideInLeft">
                <div class="main_picture">
                    <img src="../'.$img.'" alt="book" class="main_picture_img">
                </div>
            </div>
            <div class="book_describtion animate__animated animate__slideInRight">
                <div class="describtion_title">
                    <div class="book_name">'.$header.'</div>
                    <?php if ($status == 1): ?>
                      <div class="text_up_2" style="color: green;">Свободна</div>
                      <?php else: ?>
                      <div class="text_up_2" style="color: red;">Занята</div>
                <?php endif; ?>
                </div>
                <div class="book_genre">'.$kategory.'</div>
                <?php if($_SESSION[\'id\'] != $user_id): ?>
                    <a href="../user_profile/user_'.$user_id.'.php" class="describtion_btn ef">Связаться<i class="fas fa-angle-right"></i></a>
                    <?php else: ?>
                  <form method="post">
                    <button type="submit" name="change" class="extra_btn dop_eff">Изменить статус</button>
                  </form>       
                <?php endif; ?>
                <div class="describtion_text"><p>'.$text.'</p></div>
                <?php if($_SESSION[\'id\'] == $user_id): ?>
                <form method="post">
                  <button type="submit" name="delete" class="extra_btn dop_eff">Удалить объявление</button>
                </form>    
      <?php endif; ?>
            </div>
        </div>
<?php 
include \'comments.php\';
include \'../footer_down.php\';
?>'; //занесение информации, полученной из БД, для создания страницы о книге

	file_put_contents($filename, $text); //вызов функции для создания файла страницы объявления и занесение в этот файл нужной информации, полученной из БД
  }