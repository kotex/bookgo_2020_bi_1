<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Создание объявления</title>
    <link rel="stylesheet" href="add_book_style.css">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
    integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
  </head>
  <body>
  <div id="range3">  
    <div class="outer">
    <div class="middle">
    <div class="login-wr">
    <div id="logo"><i class="fas fa-book-open"></i></div>
    <h1>Создание объявления</h1>
    <form method="post" enctype="multipart/form-data">
        <p><input type="text" name="header" placeholder="Введите название книги" required></p>
        <p><textarea name="text" placeholder="Введите описание книги" required></textarea></p>
        <p>
          <select name="kategory" size="1" class="choice_of_genres">
            <option selected value="1">Классика</option>
            <option  value="2">Детектив</option>
            <option value="3">Роман</option>
            <option value="4">Фэнтези</option>
          </select>
        </p>
        <div class="add">
        <label for="myfile" class="chous">Выберите файлы<p><input type="file" name="file" class="file" id="myfile"></p></label>
        </div>
        <button type="submit"> Добавить </button>
    </form>
  </body>
</html>