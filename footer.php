    <div class="footer">
        <div class="footer_inner padding_">
            <div class="footer_top">
                <div class="footer_top_left">
                    <div class="footer_heading">Свяжись с нами</div>
                    <div class="footer_text">Нижний Новгород, Россия<br><br>Отдел технической поддержки: <br>bookgo228@gmail.com</div>
                </div>
                <div class="footer_top_middle">
                    <div class="footer_heading">Быстрое меню</div>
                    <div class="footer_middle_text">
                        <div class="middle_text">
                            <a href="index.php" class="link">Главная<br></a>
                            <a href="about_us.php" class="link">О нас<br></a>
                            <a href="catalog.php" class="link">Каталог</a>
                        </div>
                        <div class="middle_text">
                            <a href="contacts.php" class="link">Контакты<br></a>
                        <?php if ($_SESSION['id']==0): //если пользователь не авторизован, то его отправляет на страницу авторизации?>
                            <a href="authorization_script.php" class="link">Профиль<br></a>
                        <?php else: //если же пользователь авторизовался, то его отправит на страницу с его профилем?>
                            <a href="user_profile/user_<?=$_SESSION['id']?>.php" class="link">Профиль<br></a>
                        <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="footer_top_right">
                    <div class="footer_heading">Оставайся на связи!</div>
                    <div class="footer_text">Подпишись на рассылку уведомлений и узнавай больше о новинках, специальных предложениях и любимых книгах.</div>
                    <input name="emailaddress" placeholder="your email address" class="textbox" type="email" required />
                    <a href="#" class="send_btn"><img src="img/send-btn.png" alt="send"></a>
                </div>
            </div>
            <div class="footer_bottom">
                <div class="footer_bottom_text">&copy; Copyright - BOOKGO 2020. All Rights Reserved.</div>
                <div class="footer_bottom_btns">
                    <a href="https://www.facebook.com/" class="footer_btn i-1"><img src="img/facebook.png" alt="facebook"></a>
                    <a href="https://twitter.com/" class="footer_btn i-2"><img src="img/twitter.png" alt="twitter"></a>
                    <a href="https://plus.google.com/" class="footer_btn i-3"><img src="img/sm-google.png" alt="google" width="15px" height="15px"></a>
                    <a href="https://www.pinterest.ru/" class="footer_btn i-4"><img src="img/icons8-pinterest-24.png" alt="pinterest" width="15px" height="15px"></a>
                </div>
                <div class="footer_bottom_text">Terms &amp; Conditions  /  Privacy policy &amp; Cookies</div>
            </div>
        </div>
    </div>